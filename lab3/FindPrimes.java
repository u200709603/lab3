public class FindPrimes {
    public static void main(String[] args) {
        int value= Integer.parseInt(args[0]);
        int i=2;
        int j=2;
        boolean flag=true;
        while (i<=value){
            while (j<i){
                if(i%j==0) flag=false;
                j++;
            }
            if (flag){
                System.out.print(i+",");
            }
            i++;
            flag=true;
            j=2;
        }
    }
}
