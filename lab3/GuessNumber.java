import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number = rand.nextInt(100); //generates a number between 0 and 99
		boolean flag = true;
		int attempts = 1;

		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");

		int guess = reader.nextInt(); //Read the user input
		if (number == guess) System.out.println("Congratulations! ");
		else System.out.println("Sorry!");
		if (guess < number) System.out.println("Mine is greater than your guess.!");
		if (guess > number) System.out.println("Mine is less than your guess.!");
		while (flag) {
			System.out.print("Type -1 to quit or guess another: ");
			guess = reader.nextInt();
			if (guess != -1) attempts++;
			if (guess == number) {
				System.out.println("\nCongratulations! You won after " + attempts + " attempts!");
				flag = false;
			} else if (guess != -1) {
				System.out.println("Sorry!");
				if (guess < number) System.out.println("Mine is greater than your guess.!");
				if (guess > number) System.out.println("Mine is less than your guess.!");
			}
			if (guess == -1) {
				System.out.println("Sorry, the number was " + number);
				flag = false;
			}

		}
	}
}